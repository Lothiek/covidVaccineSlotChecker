data "aws_iam_policy_document" "assume_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com", "events.amazonaws.com"]
    }
  }
}

// Add Policy to allow update of security groups
data "aws_iam_policy_document" "policy" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = ["arn:aws:logs:*:*:*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "ssm:GetParameter"
    ]

    resources = ["*"]
  }
}

resource "aws_iam_role" "role" {
  name               = "iam.${var.environment}.${var.stack}.lambda.covid-vaccine-slot-checker"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_iam_role_policy" "assume_role" {
  name   = "iam.${var.environment}.${var.stack}.lambda.covid-vaccine-slot-checker"
  role   = aws_iam_role.role.id
  policy = data.aws_iam_policy_document.policy.json
}
