resource "aws_lambda_function" "lambda" {
  function_name    = "covid-vaccine-slot-checker"
  description      = "covid-vaccine-slot-checker"
  handler          = "main.lambda_handler"
  runtime          = "python3.7"
  filename         = "${path.module}/covid-vaccine-slot-checker.zip"
  source_code_hash = filebase64sha256("${path.module}/covid-vaccine-slot-checker.zip")
  role             = aws_iam_role.role.arn
  timeout          = var.lambda_timeout

  environment {
    variables = merge(var.environment_variables)
  }

  tags = {
    env        = var.environment
    managed_by = "terraform"
  }
}

resource "aws_lambda_permission" "cron" {
  statement_id  = "CronInvoke_${var.environment}_${var.stack}_covid-vaccine-slot-checker"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda.arn
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.cron.arn
}
