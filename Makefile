VENV=/tmp/covid-vaccine-slot-checker
PACKAGE=/tmp/covid-vaccine-slot-checker

all:
	rm $(VENV) -R ; true
	rm $(PACKAGE) -R ; true
	mkdir -p $(VENV)
	mkdir -p $(PACKAGE)
	cd $(VENV) && virtualenv -p python3 $(VENV)
	. $(VENV)/bin/activate ; if [ -f requirements.txt ] ; then pip install -r requirements.txt ; fi
	cp $(VENV)/lib/python*/site-packages/* $(PACKAGE)/ -r
	rm -f $(PACKAGE)/*.py*
	rm $(PACKAGE)/pkg_resources* -R
	rm $(PACKAGE)/wheel* -R
	rm $(PACKAGE)/pip* -R
	rm $(PACKAGE)/setuptools* -R
	touch $(PACKAGE)/void.dist-info
	rm $(PACKAGE)/*.dist-info -R
	find $(PACKAGE) -name "*.pyc" -exec rm {} \;
	cp app/*.py $(PACKAGE)
	cp app/config $(PACKAGE) -r
	cd $(PACKAGE) && zip -r covid-vaccine-slot-checker.zip * 1> /dev/null
	mv /$(PACKAGE)/covid-vaccine-slot-checker.zip .
