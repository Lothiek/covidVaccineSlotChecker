# Covid Vaccine Slot Checker

## Démarrage

- Vous devez disposer d'un compte sur https://pushover.net pour obtenir un "user_token"
- Vous devez créer une application pour obtenir un "api_token"
- Vous devez configurer deux variables d'environnements dans GitLabCI : AWS_ACCESS_KEY et AWS_SECRET_KEY correspondant à un utilisateur ayant les droits pour écrire sur le backend s3 et déployer des ressources IAM / SSM / LAMBDA / CLOUDWATCH EVENTS

## Notes

- Le Terraform déploie deux paramètres SSM et ignore la valeur dans le Git une fois les paramètres déployés :
    - users_config
    - users_secrets

- La configuration peut être mise à jour directement dans SSM 

### Exemple fichier users_config.json

```json
{
    "centers": [
        {
            "name": "centre-de-vaccination-covid-19-chantepie",
            "partners_url": "https://www.doctolib.fr/centre-de-sante/chantepie/centre-de-vaccination-covid-19-chantepie"
        }
    ],
    "users": [
        {
            "name": "francois",
            "additional_centers": []
        }
    ]
}
```

- "additional_centers" doit suivre le format déclaré dans "centers"

### Exemple fichier users_secrets.json

```json
{
    "francois": {
        "user_token": "",
        "api_token": ""
    }
}
```

- Chaque clef est un nom d'utilisateur qui doit correspondre au "name" déclaré dans le fichier users_config.json
- Les tokens sont obtenu

# Terraform

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

| Name | Version |
|------|---------|
| aws | 3.39.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| aws\_access\_key | aws | `string` | n/a | yes |
| aws\_region | n/a | `string` | `"eu-west-3"` | no |
| aws\_secret\_key | n/a | `string` | n/a | yes |
| client\_name | n/a | `string` | `"me"` | no |
| cron\_expression | CloudWatch cron expression | `string` | `"0/5 * * * ? *"` | no |
| environment | n/a | `string` | `"home"` | no |
| environment\_variables | Lambda environment variables | `map(any)` | <pre>{<br>  "ENABLE_TOMORROW_ALERTS": "True",<br>  "LOG_LEVEL": "DEBUG"<br>}</pre> | no |
| is\_enabled | A flag to enable Cron | `bool` | `true` | no |
| lambda\_timeout | n/a | `string` | `"300"` | no |
| stack | n/a | `string` | `"home"` | no |

## Outputs

| Name | Description |
|------|-------------|
| lambda\_arn | n/a |
| role\_arn | n/a |
| role\_name | n/a |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
