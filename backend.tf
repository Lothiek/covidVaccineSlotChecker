terraform {
  backend "s3" {
    bucket = "terraform-states-covidvaccineslotchecker"
    key    = "terraform.tfstate"
  }
}
