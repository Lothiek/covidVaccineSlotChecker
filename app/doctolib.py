#!/usr/bin/python3

import requests
import logging
from utils import now, tomorrow

logger = logging.getLogger("covid-vaccine-slot-checker")


class DoctoLib:
    def __init__(self, booking_url, availabilities_url, global_availability_date):
        self.booking_url = booking_url
        self.availabilities_url = availabilities_url
        self.global_availability_date = global_availability_date

    def load_center_infos(self, center_name):
        response = requests.get(
            f"{self.booking_url}/{center_name}.json")
        logger.debug(
            f"request : {self.booking_url}/{center_name}.json")
        logger.debug(f"response - status_code : {response.status_code}")
        requests_details = response.json()
        center_infos = {}
        if response.status_code != 200:
            logger.error(
                f"Failed to request {self.booking_url}/{center_name}.json - status_code {response.status_code}")
            return center_infos
        visit_motive_ids = [x["id"] for x in requests_details["data"]["visit_motives"]
                            if "(Pfizer-BioNTech)" in x["name"] or "(Moderna)" in x["name"]]
        agendas = [str(x["id"]) for x in requests_details["data"]["agendas"]
                   if not x["booking_disabled"]]
        practice_ids = [x["practice_id"]
                        for x in requests_details["data"]["agendas"]if not x["booking_disabled"]]
        practice_id = 0
        if practice_ids:
            practice_id = practice_ids[0]
        center_infos = {
            "center_name": center_name,
            "visit_motive_ids": visit_motive_ids,
            "agendas_ids": agendas,
            "practice_id": practice_id
        }
        logger.debug(f"Center Infos - {center_infos}")
        return center_infos

    def check_slots(self, center_infos):
        agenda_ids_fmt = "-".join(center_infos["agendas_ids"])
        practice_id = center_infos["practice_id"]
        visit_motive_ids = center_infos['visit_motive_ids'][0] if center_infos['visit_motive_ids'] else "0"
        request = f"{self.availabilities_url}?start_date={now()}&visit_motive_ids={visit_motive_ids}&agenda_ids={agenda_ids_fmt}&insurance_sector=public&practice_ids={practice_id}&destroy_temporary=true"
        response = requests.get(request)
        logger.debug(f"request - {request}")
        logger.debug(f"response - status_code : {response.status_code}")
        tomorrow_availabilities = []
        if response.status_code != 200:
            logger.error(
                f"Failed to request {request} - status_code {response.status_code}")
            return tomorrow_availabilities
        availabilities = response.json()["availabilities"]
        tomorrow_availabilities = [(x["date"], len(x["slots"]))
                                   for x in availabilities if x["slots"] and x["date"] == tomorrow()]
        logger.debug(
            f"Tomorrow availabilities : {tomorrow_availabilities}")
        return tomorrow_availabilities
