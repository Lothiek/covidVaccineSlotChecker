#!/usr/bin/python3

from datetime import datetime, timedelta


def now():
    now = datetime.now().strftime("%Y-%m-%d")
    return now


def tomorrow():
    tomorrow = datetime.now() + timedelta(days=1)
    return tomorrow.strftime("%Y-%m-%d")


def check_ga_date(slot_date, global_availability_date):
    # convert to datetime
    slot_date = datetime.strptime(slot_date, "%Y-%m-%d")
    global_availability_date = datetime.strptime(
        global_availability_date, "%Y-%m-%d")
    return slot_date >= global_availability_date


def str2bool(value):
    if str(value).lower() in ("yes", "y", "true", "t", "1"):
        return True
    if str(value).lower() in ("no", "n", "false", "f"):
        return False
