#!/usr/bin/env python3
# coding: utf-8

from logger import logger
import boto3
import json


class Ssm:
    def __init__(self, aws_region="eu-west-3"):
        self.client = boto3.client("ssm", region_name=aws_region)

    def get(self, name):
        value = json.dumps(dict())
        try:
            response = self.client.get_parameter(
                Name=name,
                WithDecryption=True
            )
            value = response['Parameter']['Value']
            logger.debug(f"SSM - get parameter - response : {response}")
            logger.info(
                f"Get SSM Parameter {name}")
        except self.client.exceptions.ParameterNotFound as err:
            logger.warning(
                f"Ssm parameter {name} not found : {err}")
            pass
        except Exception as err:
            logger.error(
                f"Failed to get ssm parameter {name} : {err}")
        return value

    def put(self, name, value):
        response = None
        try:
            response = self.client.put_parameter(
                Name=name,
                Description="",
                Value=value,
                Type='String',
                Overwrite=True,
                Tier='Standard'
            )
            logger.debug(f"SSM - put parameter - response : {response}")
            logger.info(
                f"Put SSM Parameter {name} with the value {value}")
        except Exception as err:
            logger.error(
                f"Failed to set ssm parameter {name} with the value {value} : {err}")
        return response
