#!/usr/bin/python3

from doctolib import DoctoLib
from config import Config
from logger import logger
from pushover import PushOver
from utils import str2bool
from ssm import Ssm
import json
import os


def lambda_handler(event, context):
    config = Config(path="config/config.json").load()
    enable_tomorrow_alerts = str2bool(
        os.getenv("ENABLE_TOMORROW_ALERTS", "True"))

    doctolib = DoctoLib(
        booking_url=config["doctolib_booking_url"],
        availabilities_url=config["doctolib_availabilities_url"],
        global_availability_date=config["global_availability_date"]
    )

    # get users configs
    ssm_client = Ssm()
    users_config = json.loads(ssm_client.get(name="users_config"))
    users_base_centers = users_config["centers"]
    users_secrets = json.loads(ssm_client.get(name="users_secrets"))
    tomorrow_slots = []
    for user_config in users_config["users"]:
        name = user_config["name"]
        logger.info(f"User : {name}")
        additional_centers = [
            x for x in user_config["additional_centers"] if x not in users_base_centers]
        centers = users_base_centers + additional_centers
        logger.debug(f"Merged centers list {centers}")
        enabled = str2bool(user_config["enabled"])
        logger.debug(f"User enabled ? {enabled}")
        if not enabled:
            logger.info(f"User {name} disabled")
            continue
        # get pushover user / api token
        pushover_user_token = users_secrets[name]["user_token"] if name in users_secrets else None
        pushover_api_token = users_secrets[name]["api_token"] if name in users_secrets else None

        if pushover_user_token is None or pushover_api_token is None:
            logger.warning(
                f"Failed to get pushover_user_token or pushover_api_token for user {name}")
            continue

        pushover = PushOver(
            api_url=config["pushover_api_url"],
            user_token=pushover_user_token,
            api_token=pushover_api_token
        )
        tomorrow_nb_slots = 0
        for center in centers:
            # load center details
            center_infos = doctolib.load_center_infos(
                center_name=center["name"])
            # get available slots
            tomorrow_availabilities = doctolib.check_slots(
                center_infos=center_infos
            )
            # tomorrow slots available
            tomorrow_nb_slots = sum([s for d, s in tomorrow_availabilities])
            if tomorrow_nb_slots and enable_tomorrow_alerts:
                title_html = "Dose(s) diponible(s) demain"
                message_html = f"\n<b>Centre :</b> {center['name']}\n<b>Url :</b> <a href=\"{center['partners_url']}\">Accès au centre</a>\n<b>Nombres :</b> {tomorrow_nb_slots}"
                pushover.send_message(
                    title=title_html, message=message_html, html=1)
        logger.info(f"Available slots : {tomorrow_nb_slots}")
        tomorrow_slots.append(
            {
                "name": name,
                "nb_slots": tomorrow_nb_slots
            }
        )
    return json.dumps({
        "tomorrow_slots": tomorrow_slots
    })
