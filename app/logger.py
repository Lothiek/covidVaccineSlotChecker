#!/usr/bin/env python3
# coding: utf-8

import logging
import os
import sys

stdout_handler = logging.StreamHandler(sys.stdout)
handlers = [stdout_handler]
level = logging.getLevelName(os.getenv("LOG_LEVEL", "DEBUG"))
log_format = "[%(filename)s:%(lineno)s - %(funcName)10s() ] - %(levelname)s - %(message)s"
logging.basicConfig(
    format=log_format,
    handlers=handlers,
)
logger = logging.getLogger("covid-vaccine-slot-checker")
logger.setLevel(level)
