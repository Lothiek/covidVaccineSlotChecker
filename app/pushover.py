#!/usr/bin/python3

import logging
import requests

logger = logging.getLogger("covid-vaccine-slot-checker")


class PushOver:
    def __init__(self, api_url, user_token, api_token):
        self.api_url = api_url
        self.user_token = user_token
        self.api_token = api_token

    def send_message(self, title, message, html=0):
        success = False
        data = {
            "token": self.api_token,
            "user": self.user_token,
            "title": title,
            "message": message,
            "html": html
        }
        response = requests.post(
            url=self.api_url,
            data=data
        )
        status_code = response.status_code
        logger.debug(f"response - status_code : {status_code}")
        if status_code == 200:
            success = True
        return success
