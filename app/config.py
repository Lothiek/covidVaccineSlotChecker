#!/usr/bin/python3

import json


class Config:
    def __init__(self, path):
        self.path = path

    def load(self):
        config = None
        with open(self.path, "r") as f:
            config = json.load(f)
        return config
