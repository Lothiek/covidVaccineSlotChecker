resource "aws_cloudwatch_event_rule" "cron" {
  name                = "lambda-covid-vaccine-slot-checker"
  description         = "Cron managed by Terraform."
  schedule_expression = "cron(${var.cron_expression})"
  is_enabled          = var.is_enabled
}

resource "aws_cloudwatch_event_target" "cron" {
  rule = aws_cloudwatch_event_rule.cron.name
  arn  = aws_lambda_function.lambda.arn
}
