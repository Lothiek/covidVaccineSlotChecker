# aws
variable "aws_access_key" {
  type = string
}

variable "aws_secret_key" {
  type = string
}

variable "aws_region" {
  type    = string
  default = "eu-west-3"
}

variable "environment" {
  type    = string
  default = "home"
}

variable "stack" {
  type    = string
  default = "home"
}

variable "client_name" {
  type    = string
  default = "me"
}

# lambda

variable "lambda_timeout" {
  type    = string
  default = "300"
}

variable "environment_variables" {
  description = "Lambda environment variables"
  type        = map(any)
  default = {
    LOG_LEVEL              = "DEBUG"
    ENABLE_TOMORROW_ALERTS = "True"
  }
}

variable "cron_expression" {
  description = "CloudWatch cron expression"
  type        = string
  default     = "0/5 * * * ? *"
}

variable "is_enabled" {
  description = "A flag to enable Cron"
  type        = bool
  default     = true
}
