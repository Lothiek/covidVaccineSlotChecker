output "role_arn" {
  value = aws_iam_role.role.arn
}

output "role_name" {
  value = aws_iam_role.role.name
}

output "lambda_arn" {
  value = aws_lambda_function.lambda.arn
}
