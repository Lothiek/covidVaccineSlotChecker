resource "aws_ssm_parameter" "users_config" {
  name      = "users_config"
  type      = "String"
  value     = file("users_config.json")
  overwrite = true

  lifecycle {
    ignore_changes = [value]
  }
}

resource "aws_ssm_parameter" "users_secrets" {
  name      = "users_secrets"
  type      = "String"
  value     = file("users_secrets.json")
  overwrite = true

  lifecycle {
    ignore_changes = [value]
  }

}